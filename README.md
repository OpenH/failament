# Failament

Recycling failed 3D printed stuff into new free filament!

This project is an Open Hardware project about recycling. This project is also DIY.

## Building
This project is not created yet, please wait or contribute to it. Once the project will be created the building instructions will be here.

## Troubleshooting
If you got a issue with this project, go into the *issues* tab and search if somebody else got it. If not just create one and discuss with the community about it to find a solution to your problem.

## Contribute
If you don't know how to contribute to this project and want to do so, thanks you, and this section is for you!

1. If you have an idea, go into issues to submit it. If not, find one that you like.
2. Once the issue is agreed, you can start building it by clicking on fork and doing things into your own version.
3. Once you finished, go in the original project and create a pull request by explaining your changes and your goal, you can also link the issue with it.
4. Discuss in the comments about your contribution.
5. Maybe your contribution will be merged with the actual project!

## Meta
This project has been created by OpenH and is under TAPR OHL license.  